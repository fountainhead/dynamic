UI.body.currentPath = function() {
  return Router.current().path;
};

UI.body.events({
    'click a.fancybox-fast-view': function(event, template) {
        console.log("Clicked Fast View");
        event.preventDefault();
    },
    'click a.fancybox-button': function(event, template) {
      console.log("Clicked Zoom Button");
      event.preventDefault();
    }
});

Handlebars.registerHelper('isAdminUser', function() {
    return Roles.userIsInRole(Meteor.user(), ['admin']);
});

Handlebars.registerHelper("prettifyDate", function(timestamp) {
    if (timestamp) {
        return moment(new Date(timestamp)).format("MMM-YYYY");
    }
    else {
        return ""
    };
});

Template.header.helpers({
    ROOT_URL: function() {
      return Meteor.absoluteUrl();
    }
});