// Common Form Hooks
AutoForm.addHooks(null, {
    after: {
        insert: function(error, result) {
            if (error) {
                console.log("Insert Error:", error);
                // _.each(error.invalidKeys, function(err){FlashMessages.sendError(err.message)});
            } else {
                console.log("Insert Result:", result);
            }
        },
        update: function(error) {
            if (error) {
                console.log("Update Error:", error);
            } else {
                console.log("Updated!");
                sAlert.success('Updated!', 'success');
            }
        },
        remove: function(error) {
            console.log("Remove Error:", error);
        }
    }
});
