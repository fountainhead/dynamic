Meteor.startup(function() {
    if (Meteor.isClient) {
        return SEO.config({
            title: 'Progrez',
            meta: {
                'description': 'Great Resumes Made Easy!'
            },
            og: {
                'image': 'https://pbs.twimg.com/profile_images/669359222140743680/QeF69rNj.png' 
           }
        });
    }
});