// [Simple and fancy notifications / alerts for Meteor](http://s-alert.meteor.com/)

Meteor.startup(function () {

    sAlert.config({
        position: 'bottom',
        stack: true,
        offset: 0, // in px - will be added to first alert (bottom or top - depends of the position in config)
        beep: false,
        onClose: _.noop //
    });

});