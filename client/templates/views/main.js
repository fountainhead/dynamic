import { $ } from 'meteor/jquery';

import dataTablesBootstrap from 'datatables.net-bs';
import 'datatables.net-bs/css/dataTables.bootstrap.css';
dataTablesBootstrap(window, $);

import "meteor/znewsham:dynamic-table";

Template.main.onCreated(function() {
    var self = this;
});

Template.main.helpers({
    tableOptions() {
        return {
            collection: Instruments,
            publication: "getInstruments",
            lengthChange: false,
            pageLength: 50,
            columns: [{
                    data: "tradingsymbol",
                    title: "Symbol"
                },
                {
                    data: "strike",
                    title: "Strike"
                }
            ]
        }
    },
    selector() {
        return {}
    }
});