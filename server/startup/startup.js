import GenericDb from '/server/libs/GenericDb';


Meteor.startup(() => {
    console.log('env.MONGO_URL:', process.env.MONGO_URL); // Not the URL we set in `npm run env`
});
