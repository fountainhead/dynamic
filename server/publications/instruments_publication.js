import { Meteor } from 'meteor/meteor';
import { Promise } from 'meteor/promise';


Meteor.publish("getInstruments", function(selector, options) {

    // Generated using Navicat for MongoDB
    const mySelector = {
        $and: [
            { $and: [{ "expiry": "2019-06-27" }, { "tradingsymbol": /^NIFTY.*/i }] },
            { $or: [{ "tradingsymbol": /.*CE.*/i }, { "tradingsymbol": /.*PE.*/i }] }
        ]
    };

    // To use search!
    const myNewSelector = {$and: [mySelector, selector]};


    return Instruments.find(myNewSelector, options);
});
