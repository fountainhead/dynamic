const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoDB = process.env.MONGO_URL.replace("firestone", "kite");;


const timestamps = require('mongoose-timestamp');

const orderSchema = new Schema({}, { strict: false });
const Order = mongoose.model('Order', orderSchema);


class GenericDb {
    constructor() {
        this.connection = mongoose.connection;
        this.connection.on('error', console.error.bind(console, 'connection error:'));
        this.connection.once('open', function() {
            // we're connected!
            console.log("Connected to mongodb");
        });
    }

    async connect(seconds) {
        await mongoose.connect(mongoDB, { useNewUrlParser: true });
    }

    async sayHello() {
        console.log("Say Hello!");
    }


    /**
     * Delay in seconds.
     *
     * @param seconds
     * @returns {Promise<*>}
     */
    async delay(seconds) {
        return new Promise((resolve, reject) => {

            setTimeout(() => {
                resolve("ok");
            }, seconds * 1000);
        });
    }

    // [How to do raw mongodb operations in mongoose? - Stack Overflow](https://stackoverflow.com/questions/10519432/how-to-do-raw-mongodb-operations-in-mongoose)
    // https://stackoverflow.com/a/40229995
    async generic_findOne(collection, query) {
        // https://mongodb.github.io/node-mongodb-native/3.2/reference/ecmascriptnext/crud/
        return mongoose.connection.db.collection(collection).findOne(query);
    }

    async generic_find(collection, query) {
        // https://mongodb.github.io/node-mongodb-native/3.2/reference/ecmascriptnext/crud/
        return mongoose.connection.db.collection(collection).find(query).toArray();
    }
}

module.exports = GenericDb