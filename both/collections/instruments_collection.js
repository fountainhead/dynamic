import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);
import { Tracker } from 'meteor/tracker';
SimpleSchema.debug = true;


InstrumentsSchema = new SimpleSchema({
    instrument_token: {
        type: String,
        label: 'Token',
        trim: true,
        optional: false
    },
    tradingsymbol: {
        type: String,
        label: 'Trading Symbol',
        trim: true,
        optional: true
    },
    expiry: {
        type: String,
        label: 'Expiry',
        trim: true,
        optional: true
    },
    strike: {
        type: SimpleSchema.Integer,
        label: 'Strike',
        trim: true,
        optional: true
    },
    name: {
        type: String,
        label: 'Name',
        trim: true,
        optional: true
    },
    instrument_type: {
        type: String,
        label: 'Instrument Type',
        trim: true,
        optional: true
    },
    segment: {
        type: String,
        label: 'Segment',
        trim: true,
        optional: true
    },
    lot_size: {
        type: SimpleSchema.Integer,
        label: 'Lot Size',
        trim: true,
        optional: true
    },
    createdAt: {
        type: Date,
        label: "Created At",
        optional: true,
        autoValue: function() {
            return new Date();
        }
    },
    owner: {
        type: String,
        optional: true,
        autoValue: function() {
            return this.userId;
        }
    }
}
, { tracker: Tracker });

Instruments = new Mongo.Collection("instruments");
Instruments.attachSchema(InstrumentsSchema);

//Allow, Deny rules
Instruments.allow({
    insert: function(userId, doc) {
        //return (userId && doc.owner === userId);
        return true;
    },
    update: function(userId, doc, fields, modifier) {
        // can only change your own documents
        //return doc.owner === userId;
        return true;
    },
    remove: function(userId, doc) {
        // can only remove your own documents
        return true;
    },
    fetch: ['owner']
});
