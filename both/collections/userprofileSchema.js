import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);
import { Tracker } from 'meteor/tracker';
SimpleSchema.debug = true;

Schema = {};

Schema.userprofile = new SimpleSchema({
    firstName: {
        type: String,
        label: "First Name",
        max: 100
    },
    lastName: {
        type: String,
        label: "Last Name",
        max: 100
    },
    sex: {
        type: String,
        label: "Sex",
        allowedValues: ["Male", "Female"]
    },
    dob: {
        type: Date,
        label: "Date of Birth",
        autoform: {
            type: "bootstrap-datepicker"
        }
    },
    city: {
        type: String,
        label: "City",
        max: 100
    },
    state: {
        type: String,
        label: "State",
        max: 100
    },
    twitter: {
        type: String,
        label: "Twitter",
        max: 100,
        optional: true
    },
    summary: {
        type: String,
        label: "Summary",
        max: 1000
    },
    picture: {
        type: String,
        optional: true,
        label: 'Profile picture',
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'ProfilePictures'
            }
        }
    }
});

Schema.User = new SimpleSchema({
    username: {
        type: String,
        optional: true
    },
    emails: {
        type: Array,
        optional: true
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date
    },
    userprofile: {
        type: Schema.userprofile,
        optional: true
    },
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    roles: {
        type: Array,
        blackbox: true,
        optional: true
    }
});


Meteor.users.attachSchema(Schema.User);

Meteor.users.allow({
    update: function(userId, doc, fields, modifier) {
        return true;
    }
});