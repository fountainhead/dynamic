// Config files for Account Templates
// [core/Guide.md at master · meteor-useraccounts/core](https://github.com/meteor-useraccounts/core/blob/master/Guide.md)

AccountsTemplates.configure({
    // Behaviour
    confirmPassword: true,
    enablePasswordChange: true,
    forbidClientAccountCreation: false,
    overrideLoginErrors: true,
    sendVerificationEmail: false,
    lowercaseUsername: false,

    // Appearance
    showAddRemoveServices: false,
    showForgotPasswordLink: true,
    showLabels: true,
    showPlaceholders: true,
    showResendVerificationEmailLink: false,

    // Client-side Validation
    continuousValidation: false,
    negativeFeedback: false,
    negativeValidation: true,
    positiveValidation: true,
    positiveFeedback: true,
    showValidating: true,

    // Privacy Policy and Terms of Use
    privacyUrl: 'privacy',
    termsUrl: 'terms-of-use',

    // Redirects
    homeRoutePath: '/',
    redirectTimeout: 4000,

    // Texts
    texts: {
      button: {
          signUp: "Register Now!"
      },
      title: {
          forgotPwd: "Recover Your Password"
      },
    },
});


AccountsTemplates.configureRoute('signIn', {
    redirect: '/',
    template: 'login',
    layoutTemplate: 'blankLayout',
});

AccountsTemplates.configureRoute('signUp', {
    redirect: '/profile',
    template: 'login',
    layoutTemplate: 'blankLayout',
});

AccountsTemplates.configureRoute('forgotPwd', {
});

AccountsTemplates.configureRoute('changePwd', {
});

AccountsTemplates.configureRoute('resetPwd', {
    template: 'login',
    layoutTemplate: 'blankLayout'
});

AccountsTemplates.configureRoute('enrollAccount', {
    template: 'login',
    layoutTemplate: 'blankLayout',
});

AccountsTemplates.configureRoute('verifyEmail', {
});