Router.map(function() {
    // Edit profile
    this.route('profile', {
        path: '/profile',
        controller: 'LoggedUserController',
        waitOn: function() {
            Meteor.subscribe("profilePictures");
        },
        data: function() {
            return Meteor.user();
        },
        onAfterAction: function () {
        }
    });
});